# Cyclone Physics Engine

Work in progress. Written by following the ["Game Physics Engine Development" book by Ian Millington](https://www.sciencedirect.com/book/9780123819765/game-physics-engine-development).

## Goals?

With this particular engine, I'm not sure. I already use it in a toy 2D engine I'm writing and it works rather well already.
At a pinch I would say that an easily extenable physics engine with with most common primitives supported would be the goal,
but there is a long way to go yet.

Currently simple particle style physics is usable, and you can make reasonable physics out of them already.

# License

Because this largely follows the code presented by Ian Millington which is under MIT license, this project is licensed the same.
