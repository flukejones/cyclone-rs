use crate::force_generators::ForceGenerator;

/// Contains the registration of all `ForceGenerators` in the
/// engine, each force registered is updated in turn at each
/// physics 'tick'
pub struct ForceRegistry {
    registrations: Vec<Box<dyn ForceGenerator>>,
}

impl ForceRegistry {
    pub fn new() -> ForceRegistry {
        ForceRegistry {
            registrations: Vec::new(),
        }
    }

    /// Add the created `ForceGenerator` to the registry
    pub fn add(&mut self, fg: impl ForceGenerator + 'static) {
        self.registrations.push(Box::new(fg));
    }

    /// Remove all registered forces containing the requested ID
    pub fn remove_if_contains(&mut self, ptr: usize) {
        'removal: loop {
            let mut index = 0;
            for (i, fg) in self.registrations.iter().enumerate() {
                if fg.contains_ptr(ptr) {
                    index = i;
                    break;
                }
                if i >= self.registrations.len() - 1 {
                    break 'removal;
                }
            }
            self.registrations.remove(index);
        }
    }

    /// Clear all forces from registry
    pub fn clear(&mut self) {
        self.registrations.clear()
    }

    /// Calls the `update_force()` method on all forces in the registry
    pub fn update_forces(&mut self) {
        for fg in &mut self.registrations {
            fg.update_force();
        }
    }
}
