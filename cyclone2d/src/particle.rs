use crate::vek2d::Vec2d;
use crate::FP;
use std::ptr;

#[derive(Debug)]
pub struct Particle {
    position:     Vec2d<FP>,
    velocity:     Vec2d<FP>,
    mass:         FP,
    inv_mass:     FP,
    force_accum:  Vec2d<FP>,
    acceleration: Vec2d<FP>,
    damping:      FP,
}

impl Particle {
    /// Create a new particle object with a mass and damping factor.
    /// Damping is required due to floating-point precision - good
    /// values depend on your desired effect, with `0.999` being
    /// a very low damping value, and 0.01 being high.
    pub fn new(mass: FP, damping: FP) -> Particle {
        let inv_mass = if mass == 0.0 { 0.0 } else { 1.0 / mass };
        Particle {
            position: Vec2d::default(),
            velocity: Vec2d::default(),
            mass,
            inv_mass,
            force_accum: Vec2d::default(),
            acceleration: Vec2d::default(),
            damping,
        }
    }

    #[inline]
    pub fn position(&self) -> Vec2d<FP> {
        self.position
    }

    #[inline]
    pub fn set_position(&mut self, xy: Vec2d<FP>) {
        self.position = xy;
    }

    #[inline]
    pub fn velocity(&self) -> Vec2d<FP> {
        self.velocity
    }

    #[inline]
    pub fn set_velocity(&mut self, xy: Vec2d<FP>) {
        self.velocity = xy;
    }

    #[inline]
    pub fn mass(&self) -> FP {
        self.mass
    }

    #[inline]
    pub fn set_mass(&mut self, mass: FP) {
        self.mass = mass;
        self.inv_mass = if mass == 0.0 { 0.0 } else { 1.0 / mass };
    }

    #[inline]
    pub fn inverse_mass(&self) -> FP {
        self.inv_mass
    }

    #[inline]
    pub fn acceleration(&self) -> Vec2d<FP> {
        self.acceleration
    }

    #[inline]
    pub fn set_acceleration(&mut self, xy: Vec2d<FP>) {
        self.acceleration = xy;
    }

    #[inline]
    pub fn damping(&self) -> FP {
        self.damping
    }

    #[inline]
    pub fn set_damping(&mut self, damping: FP) {
        self.damping = damping;
    }

    #[inline]
    pub fn forces(&self) -> Vec2d<FP> {
        self.force_accum
    }

    #[inline]
    pub fn add_force(&mut self, force: Vec2d<FP>) {
        self.force_accum += force;
    }

    #[inline]
    pub fn clear_forces(&mut self) {
        self.force_accum.clear();
    }

    #[inline]
    pub fn as_non_null_ptr(&mut self) -> ptr::NonNull<Particle> {
        let x: *mut Particle = self;
        unsafe { ptr::NonNull::new_unchecked(x) }
    }
}

impl Default for Particle {
    #[inline]
    fn default() -> Particle {
        Self::new(1.0, 0.999)
    }
}
