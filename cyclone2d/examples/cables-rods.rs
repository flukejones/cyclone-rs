use sdl2::event::Event;
use sdl2::gfx::primitives::{DrawRenderer, ToColor};
use sdl2::keyboard::Keycode;

use cyclone2d::contact_generators::{
    Cable, CableConstraint, GroundContact, Rod, RodConstraint,
};
use cyclone2d::particle::Particle;
use cyclone2d::vek2d::Vec2d;
use cyclone2d::world::World;
use cyclone2d::FP;

fn setup_world(particle_world: &mut World) {
    for i in 0..4 {
        let mut particle = Particle::new(1.0, 0.99);
        particle.set_acceleration(Vec2d::new(0.0, -9.81));
        if i == 0 || i == 2 {
            particle.set_position(Vec2d::new(200.0, 550.0 + i as FP * 40.0));
        } else {
            particle.set_position(Vec2d::new(600.0, 550.0 + i as FP * 40.0));
        }
        let x = particle.position().x();
        let pid = particle_world.add_particle(particle).unwrap();
        let this_ptr = particle_world.get_particle_ptr(pid).unwrap();

        if i != 0 && i != 1 {
            particle_world.add_contact_generator(CableConstraint::new(
                this_ptr,
                Vec2d::new(x, 590.0),
                400.0,
                0.3,
            ));
        } else if i != 1 {
            particle_world.add_contact_generator(RodConstraint::new(
                this_ptr,
                Vec2d::new(x, 590.0),
                200.0,
                0.3,
            ));
        }
        if i == 1 {
            let link_ptr = particle_world.get_particle_ptr(0).unwrap();
            particle_world.add_contact_generator(Cable::new(
                this_ptr, link_ptr, 270.0, 0.3,
            ));
        }
        if i == 3 {
            let link_ptr = particle_world.get_particle_ptr(2).unwrap();
            particle_world.add_contact_generator(Rod::new(
                this_ptr, link_ptr, 400.0, 0.0,
            ));
        }
    }
    particle_world.add_contact_generator(GroundContact::new());
}

fn draw(
    canvas: &mut sdl2::render::Canvas<sdl2::video::Window>,
    particle_world: &World,
) {
    let red = (255, 100, 100, 255);
    // clear background to black
    canvas.set_draw_color(sdl2::pixels::Color::RGBA(0, 0, 0, 255));
    canvas.clear();

    // draw particles
    for (i, v) in particle_world.get_particle_array().iter().enumerate() {
        let mut x1;
        let mut y1;
        let mut x2;
        let mut y2;
        if let Some(v) = v {
            if i != 0 && i != 2 {
                x1 = 800 - v.position().x() as i16;
                y1 = 600 - v.position().y() as i16;
                let end = particle_world.get_particle_position(i - 1).unwrap();
                x2 = 800 - end.x() as i16;
                y2 = 600 - end.y() as i16;
                canvas.thick_line(x1, y1, x2, y2, 5, red.as_rgba()).unwrap();
            }
            if i == 0 || i == 2 {
                x1 = 800 - v.position().x() as i16;
                y1 = 600 - v.position().y() as i16;
                x2 = 800 - 200;
                y2 = 10;
                canvas.thick_line(x1, y1, x2, y2, 5, red.as_rgba()).unwrap();
            }
            if i == 3 {
                x1 = 800 - v.position().x() as i16;
                y1 = 600 - v.position().y() as i16;
                x2 = 200;
                y2 = 10;
                canvas.thick_line(x1, y1, x2, y2, 5, red.as_rgba()).unwrap();
            }
        }
    }
    canvas.present();
}

fn main() -> Result<(), String> {
    let sdl_context = sdl2::init()?;
    let video_subsystem = sdl_context.video()?;

    let window = video_subsystem
        .window("SDL2", 800, 600)
        .position_centered()
        .build()
        .map_err(|e| e.to_string())?;

    let mut canvas = window
        .into_canvas()
        .accelerated()
        .build()
        .map_err(|e| e.to_string())?;

    let mut timer = sdl_context.timer()?;
    let mut event_pump = sdl_context.event_pump()?;

    // Set up particles and world
    let mut particle_world = World::new(100, 1000, None);
    setup_world(&mut particle_world);

    const MS_PER_UPDATE: FP = 4.0;
    let mut last_tick = timer.ticks();
    let mut this_tick;
    let mut lag = 0.0;

    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => {
                    break 'running;
                }
                _ => {}
            }
        }

        this_tick = timer.ticks();
        lag += (this_tick - last_tick) as FP;
        last_tick = this_tick;

        while lag >= MS_PER_UPDATE {
            particle_world.start_frame();
            particle_world.run_physics(MS_PER_UPDATE as FP * 0.01);
            lag -= MS_PER_UPDATE;
        }
        draw(&mut canvas, &particle_world);
    }
    Ok(())
}
