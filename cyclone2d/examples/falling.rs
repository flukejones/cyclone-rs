use sdl2::event::Event;
use sdl2::gfx::primitives::{DrawRenderer, ToColor};
use sdl2::keyboard::Keycode;

use cyclone2d::contact_generators::GroundContact;
use cyclone2d::force_generators::{Buoyancy, Drag, Gravity};
use cyclone2d::particle::Particle;
use cyclone2d::vek2d::Vec2d;
use cyclone2d::world::World;
use cyclone2d::FP;

fn main() -> Result<(), String> {
    let sdl_context = sdl2::init()?;
    let video_subsystem = sdl_context.video()?;

    let window = video_subsystem
        .window("SDL2", 800, 600)
        .position_centered()
        .build()
        .map_err(|e| e.to_string())?;

    let mut canvas = window
        .into_canvas()
        .accelerated()
        .present_vsync()
        .build()
        .map_err(|e| e.to_string())?;

    let mut timer = sdl_context.timer()?;
    let mut event_pump = sdl_context.event_pump()?;

    // Set up particles and world
    let mut particle_world = World::new(100, 1000, None);
    for i in 1..8 {
        let mut particle = Particle::new(1.0, 0.999);
        particle.set_position(Vec2d::new(0.0 + i as FP * 100.0, 500.0));
        particle.set_mass(1.0 + i as FP * 20.0);
        let pid;
        if i != 7 {
            pid = particle_world.add_particle(particle).unwrap();
        } else {
            // Buoyancy particle 200 mass
            particle.set_mass(220.0);
            particle.set_damping(0.85);
            pid = particle_world.add_particle(particle).unwrap();
            let ptr = particle_world.get_particle_ptr(pid).unwrap();
            particle_world.add_force_generator(Buoyancy::new(
                ptr, 1.3, 2.4, 400.0, 1000.0,
            ));
        }

        let ptr = particle_world.get_particle_ptr(pid).unwrap();
        particle_world.add_force_generator(Gravity::new(ptr, -9.81));
        particle_world.add_force_generator(Drag::new(ptr, 0.001, 0.001));
    }

    particle_world.add_contact_generator(GroundContact::new());

    let red = (255, 100, 100, 255);
    let grey = (150, 150, 150, 255);

    const MS_PER_UPDATE: FP = 2.0;
    let mut last_tick = timer.ticks();
    let mut this_tick;
    let mut lag = 0.0;

    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => {
                    break 'running;
                }
                _ => {}
            }
        }

        this_tick = timer.ticks();
        lag += (this_tick - last_tick) as FP;
        last_tick = this_tick;

        while lag >= MS_PER_UPDATE {
            particle_world.start_frame();
            particle_world.run_physics(MS_PER_UPDATE as FP * 0.01);
            lag -= MS_PER_UPDATE;
        }

        // clear background to black
        canvas.set_draw_color(sdl2::pixels::Color::RGBA(0, 0, 0, 255));
        canvas.clear();

        // water line
        canvas.line(50, 200, 150, 200, red.as_rgba())?;
        // draw particles
        for v in particle_world.get_particle_array() {
            if let Some(v) = v {
                canvas.filled_circle(
                    800 - v.position().x() as i16,
                    590 - v.position().y() as i16,
                    10,
                    grey.as_rgba(),
                )?;
            }
        }
        canvas.present();
    }
    Ok(())
}
