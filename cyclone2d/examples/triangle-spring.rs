use sdl2::event::Event;
use sdl2::gfx::primitives::{DrawRenderer, ToColor};
use sdl2::keyboard::Keycode;

use cyclone2d::contact_generators::{GroundContact, Rod};
use cyclone2d::force_generators::AnchoredSpring;
use cyclone2d::particle::Particle;
use cyclone2d::vek2d::Vec2d;
use cyclone2d::world::World;
use cyclone2d::FP;

use std::time::Instant;

#[derive(Debug)]
pub struct TimeStep {
    last_time:   Instant,
    delta_time:  FP,
    frame_count: u32,
    frame_time:  FP,
}

impl Default for TimeStep {
    fn default() -> TimeStep {
        TimeStep {
            last_time:   Instant::now(),
            delta_time:  0.0,
            frame_count: 0,
            frame_time:  0.0,
        }
    }
}
impl TimeStep {
    pub fn delta(&mut self) -> FP {
        let current_time = Instant::now();
        let delta = current_time.duration_since(self.last_time).subsec_micros()
            as FP
            * 0.001;
        self.last_time = current_time;
        self.delta_time = delta;
        delta
    }

    pub fn frame_rate(&mut self) -> Option<u32> {
        self.frame_count += 1;
        self.frame_time += self.delta_time;
        let tmp;
        if self.frame_time >= 1000.0 {
            tmp = self.frame_count;
            self.frame_count = 0;
            self.frame_time = 0.0;
            return Some(tmp);
        }
        None
    }
}

fn setup_world(particle_world: &mut World) {
    let mut particle = Particle::new(1.0, 0.95);
    particle.set_acceleration(Vec2d::new(0.0, -9.81));
    particle.set_position(Vec2d::new(100.0, 500.0));
    let pid0 = particle_world.add_particle(particle).unwrap();
    let ptr0 = particle_world.get_particle_ptr(pid0).unwrap();

    let mut particle = Particle::new(1.0, 0.95);
    particle.set_acceleration(Vec2d::new(0.0, -9.81));
    particle.set_position(Vec2d::new(300.0, 500.0));
    let pid1 = particle_world.add_particle(particle).unwrap();
    let ptr1 = particle_world.get_particle_ptr(pid1).unwrap();

    let mut particle = Particle::new(2.5, 0.95);
    particle.set_acceleration(Vec2d::new(0.0, -9.81));
    particle.set_position(Vec2d::new(300.0, 700.0));
    let pid2 = particle_world.add_particle(particle).unwrap();
    let ptr2 = particle_world.get_particle_ptr(pid2).unwrap();

    particle_world.add_contact_generator(Rod::new(ptr0, ptr1, 150.0, 0.0));
    particle_world.add_contact_generator(Rod::new(ptr1, ptr2, 100.0, 0.0));
    particle_world.add_contact_generator(Rod::new(ptr2, ptr0, 200.0, 0.0));

    particle_world.add_force_generator(AnchoredSpring::new(
        ptr1,
        Vec2d::new(400.0, 600.0),
        0.3,
        230.0,
    ));

    particle_world.add_contact_generator(GroundContact::new());
}

fn draw(
    canvas: &mut sdl2::render::Canvas<sdl2::video::Window>,
    particle_world: &World,
) {
    let red = (255, 100, 100, 255);
    // clear background to black
    canvas.set_draw_color(sdl2::pixels::Color::RGBA(0, 0, 0, 255));
    canvas.clear();
    // draw particles
    for (i, v) in particle_world.get_particle_array().iter().enumerate() {
        if let Some(v) = v {
            if i == 1 {
                let x1 = 800 - v.position().x() as i16;
                let y1 = 600 - v.position().y() as i16;
                canvas.thick_line(x1, y1, 400, 0, 5, red.as_rgba()).unwrap();
            }
            if i != 2 {
                let x1 = 800 - v.position().x() as i16;
                let y1 = 600 - v.position().y() as i16;
                let end = particle_world.get_particle_position(i + 1).unwrap();
                let x2 = 800 - end.x() as i16;
                let y2 = 600 - end.y() as i16;
                canvas.thick_line(x1, y1, x2, y2, 5, red.as_rgba()).unwrap();
            } else {
                let x1 = 800 - v.position().x() as i16;
                let y1 = 600 - v.position().y() as i16;
                let end = particle_world.get_particle_position(0).unwrap();
                let x2 = 800 - end.x() as i16;
                let y2 = 600 - end.y() as i16;
                canvas.thick_line(x1, y1, x2, y2, 5, red.as_rgba()).unwrap();
            }
        }
    }
    canvas.present();
}

fn main() -> Result<(), String> {
    let sdl_context = sdl2::init()?;
    let video_subsystem = sdl_context.video()?;

    let window = video_subsystem
        .window("SDL2", 800, 600)
        .position_centered()
        .build()
        .map_err(|e| e.to_string())?;

    let mut canvas = window
        .into_canvas()
        .accelerated()
        .present_vsync()
        .build()
        .map_err(|e| e.to_string())?;

    let mut event_pump = sdl_context.event_pump()?;

    // Set up particles and world
    let mut particle_world = World::new(100, 1000, None);
    setup_world(&mut particle_world);

    const MS_PER_UPDATE: FP = 2.0;
    let mut lag = 0.0;
    let mut timestep = TimeStep::default();

    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => {
                    break 'running;
                }
                _ => {}
            }
        }

        let delta = timestep.delta();
        lag += delta;

        while lag >= MS_PER_UPDATE {
            particle_world.start_frame();
            particle_world.run_physics(MS_PER_UPDATE as FP * 0.01);
            lag -= MS_PER_UPDATE;
        }

        draw(&mut canvas, &particle_world);
        if let Some(t) = timestep.frame_rate() {
            println!("FPS:{}", t);
        }
    }
    Ok(())
}
