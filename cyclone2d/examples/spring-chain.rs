use sdl2::event::Event;
use sdl2::gfx::primitives::{DrawRenderer, ToColor};
use sdl2::keyboard::Keycode;

use cyclone2d::contact_generators::GroundContact;
use cyclone2d::force_generators::{AnchoredBungee, AnchoredSpring, Gravity};
use cyclone2d::particle::Particle;
use cyclone2d::vek2d::Vec2d;
use cyclone2d::world::World;
use cyclone2d::FP;

fn setup_world(particle_world: &mut World, count: usize) {
    let tension = 0.9;
    let distance = 1.0;
    for i in 0..=count {
        let mut particle = Particle::new(2.3, 0.9);
        particle.set_position(Vec2d::new(100.0 + (i as FP * 50.0), 550.0));
        let pid = particle_world.add_particle(particle).unwrap();
        let this_ptr = particle_world.get_particle_ptr(pid).unwrap();
        if i != 0 {
            let link_ptr = particle_world.get_particle_ptr(pid - 1).unwrap();
            particle_world.add_force_generator(AnchoredBungee::new(
                this_ptr, link_ptr, tension, distance,
            ));
        }

        if i == 0 {
            // anchor one particle
            particle_world.add_force_generator(AnchoredSpring::new(
                this_ptr,
                Vec2d::new(100.0, 550.0),
                tension,
                distance,
            ));
        }
        if i == count {
            // anchor one particle
            particle_world.add_force_generator(AnchoredSpring::new(
                this_ptr,
                Vec2d::new(700.0, 550.0),
                tension,
                distance,
            ));
        }
        // try removing gravity or reducing it
        particle_world.add_force_generator(Gravity::new(this_ptr, -9.81));
        //particle_world.add_force_generator(
        //    Drag::new(i, 0.01, 0.03)));
    }

    for i in 0..=count {
        if i != count {
            let this_ptr = particle_world.get_particle_ptr(i).unwrap();
            let link_ptr = particle_world.get_particle_ptr(i + 1).unwrap();
            particle_world.add_force_generator(AnchoredBungee::new(
                this_ptr, link_ptr, tension, distance,
            ));
        }
    }

    particle_world.add_contact_generator(GroundContact::new());
}

fn draw(
    canvas: &mut sdl2::render::Canvas<sdl2::video::Window>,
    particle_world: &World,
    count: usize,
) {
    let red = (255, 100, 100, 255);
    // clear background to black
    canvas.set_draw_color(sdl2::pixels::Color::RGBA(0, 0, 0, 255));
    canvas.clear();

    // centered circle
    canvas.filled_circle(100, 50, 10, red.as_rgba()).unwrap();
    canvas.filled_circle(700, 50, 10, red.as_rgba()).unwrap();
    // draw particles
    for (i, v) in particle_world.get_particle_array().iter().enumerate() {
        if let Some(v) = v {
            let mut x1;
            let mut y1;
            let mut x2;
            let mut y2;
            if i == 0 {
                x1 = 700;
                y1 = 50;
                x2 = 800 - v.position().x() as i16;
                y2 = 600 - v.position().y() as i16;
                canvas.thick_line(x1, y1, x2, y2, 5, red.as_rgba()).unwrap();
            } else {
                x1 = 800 - v.position().x() as i16;
                y1 = 600 - v.position().y() as i16;
                let end = particle_world.get_particle_position(i - 1).unwrap();
                x2 = 800 - end.x() as i16;
                y2 = 600 - end.y() as i16;
                canvas.thick_line(x1, y1, x2, y2, 5, red.as_rgba()).unwrap();
            }
            if i == count {
                x1 = 800 - v.position().x() as i16;
                y1 = 600 - v.position().y() as i16;
                x2 = 100;
                y2 = 50;
                canvas.thick_line(x1, y1, x2, y2, 5, red.as_rgba()).unwrap();
            }
        }
    }
    canvas.present();
}

fn main() -> Result<(), String> {
    let sdl_context = sdl2::init()?;
    let video_subsystem = sdl_context.video()?;

    let window = video_subsystem
        .window("SDL2", 800, 600)
        .position_centered()
        .build()
        .map_err(|e| e.to_string())?;

    let mut canvas = window
        .into_canvas()
        .accelerated()
        .present_vsync()
        .build()
        .map_err(|e| e.to_string())?;

    let mut timer = sdl_context.timer()?;
    let mut event_pump = sdl_context.event_pump()?;

    // Set up particles and world
    let mut particle_world = World::new(100, 1000, None);
    let count = 10;
    setup_world(&mut particle_world, count);

    const MS_PER_UPDATE: FP = 4.0;
    let mut last_tick = timer.ticks();
    let mut this_tick;
    let mut lag = 0.0;

    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => {
                    break 'running;
                }
                _ => {}
            }
        }

        this_tick = timer.ticks();
        lag += (this_tick - last_tick) as FP;
        last_tick = this_tick;

        while lag >= MS_PER_UPDATE {
            particle_world.start_frame();
            particle_world.run_physics(MS_PER_UPDATE as FP * 0.01);
            lag -= MS_PER_UPDATE;
        }

        draw(&mut canvas, &particle_world, count);
    }
    Ok(())
}
